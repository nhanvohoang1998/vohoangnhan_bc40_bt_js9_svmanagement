function SinhVien(
    _ma,
    _ten,
    _email,
    _password,
    _diemToan,
    _diemLy,
    _diemHoa,
) {
    this.ma = _ma;
    this.ten = _ten;
    this.email = _email;
    this.password = _password;
    this.diemToan = _diemToan;
    this.diemLy = _diemLy;
    this.diemHoa = _diemHoa;
    this.DTB = function () {
        return (Number(this.diemHoa) + Number(this.diemToan) + Number(this.diemLy)) / 3
    };
}