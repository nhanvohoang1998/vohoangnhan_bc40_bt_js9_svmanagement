var dssv = []
data = localStorage.getItem("DSSV")
if (data != null) {
    var arrdssv = JSON.parse(data)
    dssv = arrdssv.map(function (item) {
        return new SinhVien(
            item.ma,
            item.ten,
            item.email,
            item.password,
            item.diemToan,
            item.diemLy,
            item.diemHoa,
        )
    })
    renderSV(dssv)
}

function themSV() {
    var sv = layThongTinSVTuForm();
    dssv.push(sv);
    var data = JSON.stringify(dssv)
    localStorage.setItem("DSSV", data)
    console.log("🚀 ~ file: main.js:6 ~ themSV ~ dssv", dssv)
    renderSV(dssv)
}

function xoaSV(id) {
    console.log("🚀 ~ file: main.js:29 ~ xoaSV ~ id", id)
    var viTri = timViTriTrongDSSV(id, dssv)
    console.log("🚀 ~ file: main.js:31 ~ xoaSV ~ viTri", viTri)
    if (viTri != -1) {
        dssv.splice(viTri, 1)
        var data = JSON.stringify(dssv)
        localStorage.setItem("DSSV", data)
        renderSV(dssv)
    }
}

function suaSV(id) {
    var viTri = timViTriTrongDSSV(id, dssv)
    updateThongTinLenForm(viTri, dssv)
}

function capNhatSV() {
    var sv = layThongTinSVTuForm()
    var viTri = timViTriTrongDSSV(sv.ma, dssv)
    if (viTri != -1) {
        dssv[viTri] = sv
        var data = JSON.stringify(dssv)
        localStorage.setItem("DSSV", data)
        renderSV(dssv)
    }
}