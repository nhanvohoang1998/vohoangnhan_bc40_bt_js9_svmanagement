function layThongTinSVTuForm() {
    _ma = document.getElementById("txtMaSV").value;
    _ten = document.getElementById("txtTenSV").value;
    _email = document.getElementById("txtEmail").value;
    _password = document.getElementById("txtPass").value;
    _diemToan = document.getElementById("txtDiemToan").value;
    _diemLy = document.getElementById("txtDiemLy").value;
    _diemHoa = document.getElementById("txtDiemHoa").value;

    var sv = new SinhVien(
        _ma,
        _ten,
        _email,
        _password,
        _diemToan,
        _diemLy,
        _diemHoa,
    )
    return sv
}

function renderSV(arr) {
    var danhSachRender = ""
    for (var i = 0; i < arr.length; i++) {
        var contentTr = `<tr>
                            <td>${arr[i].ma}</td>
                            <td>${arr[i].ten}</td>
                            <td>${arr[i].email}</td>
                            <td>${arr[i].DTB()}</td>
                            <td>
                                <button class="btn btn-danger" onclick="xoaSV(${arr[i].ma})">Xóa</button>
                                <button class="btn btn-warning" onclick="suaSV(${arr[i].ma})">Sửa</button>
                            </td>
                        </tr>`
        danhSachRender += contentTr
    }
    document.getElementById("tbodySinhVien").innerHTML = danhSachRender
}

function timViTriTrongDSSV(id, dssv) {
    var viTri = -1
    for (var i = 0; i < dssv.length; i++) {
        if (dssv[i].ma == id) {
            viTri = i;
            break
        }
    }
    return viTri
}

function updateThongTinLenForm(viTri, dssv) {
    document.getElementById("txtMaSV").value = dssv[viTri].ma
    document.getElementById("txtTenSV").value = dssv[viTri].ten
    document.getElementById("txtEmail").value = dssv[viTri].email
    document.getElementById("txtPass").value = dssv[viTri].password
    document.getElementById("txtDiemToan").value = dssv[viTri].diemToan
    document.getElementById("txtDiemLy").value = dssv[viTri].diemLy
    document.getElementById("txtDiemHoa").value = dssv[viTri].diemHoa
}